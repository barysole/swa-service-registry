# SWA - Semestral work - ServiceRegistry

Author: Oleg Baryshnikov


## Description
This application works as a service register to the root project - https://gitlab.fel.cvut.cz/barysole/swa-root .
The service register provides client-side service discovery possibilities which allows services to find and communicate with each other without hard-coding the hostname and port. Application works on "8761" port. Every service from the root project tries to register here.
## GitLab pipeline
Project contains their own GitLab pipeline described in .gitlab-ci.yml. Gitlab pipeline contains two stages: build and building image.

Stage description:
- On a build stage the runner builds jar artefacts and stores them for further uses.
- On a building stage runner build docker-image and push it to following private repository: https://hub.docker.com/repository/docker/verminaardo/swa-baryshnikov-service-register
## Most important app properties
```
spring.application.name=SwaServiceRegistry
server.port=8761
```